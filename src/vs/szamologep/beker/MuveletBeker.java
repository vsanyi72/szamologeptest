package vs.szamologep.beker;

import java.io.InputStream;
import java.util.Scanner;

public class MuveletBeker implements Beker<String>{

	private Scanner sc;
	
	public MuveletBeker(InputStream is) {
		this.sc = new Scanner(is);
	}
	
	@Override
	public String beker() {
		return this.sc.nextLine();
	}

}
