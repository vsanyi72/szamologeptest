package vs.szamologep.beker;

import java.io.InputStream;
import java.util.Scanner;

public class SzamBeker implements Beker<Double>{

	private Scanner sc;
	
	public SzamBeker(InputStream is) {
		this.sc = new Scanner(is);
	}
	
	@Override
	public Double beker() {
		return this.sc.nextDouble();
	}

}
