package vs.szamologep;

import vs.szamologep.muvelet.Kivon;
import vs.szamologep.muvelet.Muvelet;
import vs.szamologep.muvelet.Osszead;

public class Szamologep {
	
	private String muvelet;
	private double szam1;
	private double szam2;
	
	public Szamologep() { }
	
	
	public double szamol() {
		Muvelet m = null;
		
		switch (muvelet) {
		case "+":
			m = new Osszead(szam1, szam2);
			break;
		case "-":
			m = new Kivon(szam1, szam2);
			break;

		default:
			break;
		}
		
		return m.szamol();
	}
	
	public String getMuvelet() {
		return muvelet;
	}

	public void setMuvelet(String muvelet) {
		this.muvelet = muvelet;
	}

	public double getSzam1() {
		return szam1;
	}

	public void setSzam1(double szam1) {
		this.szam1 = szam1;
	}

	public double getSzam2() {
		return szam2;
	}

	public void setSzam2(double szam2) {
		this.szam2 = szam2;
	}

	
}
