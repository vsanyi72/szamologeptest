package vs.szamologep.muvelet;

public class Kivon implements Muvelet 
{
	private double a;
	private double b;
	
	public Kivon(double a, double b)
	{
		this.a=a;
		this.b=b;
	}
	
	@Override
	public double szamol() 
	{
		return this.a-this.b;
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

}
