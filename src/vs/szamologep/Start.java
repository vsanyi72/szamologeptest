package vs.szamologep;

import vs.szamologep.beker.MuveletBeker;
import vs.szamologep.beker.SzamBeker;

public class Start {

	public static void main(String[] args) {
		
		Szamologep szamologep = new Szamologep();
		System.out.println("Kerem a muveletet : ");
		String muvelet = new MuveletBeker(System.in).beker();
		System.out.println("Kerem az elso szamot :");
		double szam1 = new SzamBeker(System.in).beker();
		System.out.println("Kerem a masodik szamot :");
		double szam2 = new SzamBeker(System.in).beker();
		
		szamologep.setMuvelet(muvelet);
		szamologep.setSzam1(szam1);
		szamologep.setSzam2(szam2);
		
		System.out.println("Eredmény: "+szamologep.szamol());
	}

}
