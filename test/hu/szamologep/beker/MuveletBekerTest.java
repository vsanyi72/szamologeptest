package hu.szamologep.beker;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import vs.szamologep.beker.MuveletBeker;

class MuveletBekerTest {

	private MuveletBeker muveletBeker;
	private InputStream is;
	
	
	@Test
	void testOsszead() {
		this.is = new ByteArrayInputStream("+".getBytes());
		this.muveletBeker = new MuveletBeker(this.is);
		String muvelet=this.muveletBeker.beker();
		assertEquals(muvelet, "+");
		
	}
	
	@Test
	void testKivon() {
		this.is = new ByteArrayInputStream("-".getBytes());
		this.muveletBeker = new MuveletBeker(this.is);
		String muvelet=this.muveletBeker.beker();
		assertEquals(muvelet, "-");
		
	}

}
