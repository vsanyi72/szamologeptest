package hu.szamologep.beker;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import vs.szamologep.beker.SzamBeker;

class SzamBekerTest {

	private SzamBeker szamBeker;
	private InputStream is;
	
	@Test
	void positivBekerTest() {
		this.is = new ByteArrayInputStream("34".getBytes());
		this.szamBeker = new SzamBeker(this.is);
		double muvelet=this.szamBeker.beker();
		assertEquals(muvelet, 34);
		assertNotEquals(muvelet, -34);
	}

}
