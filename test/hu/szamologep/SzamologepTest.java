package hu.szamologep;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vs.szamologep.Szamologep;

class SzamologepTest {
	
	private Szamologep szamologep;

	@Test
	void test() {
		this.szamologep = new Szamologep();
		this.szamologep.setMuvelet("+");
		this.szamologep.setSzam1(3);
		this.szamologep.setSzam2(3);
		assertEquals(this.szamologep.szamol(), 6);
	}
	
	@Test
	void test2() {
		this.szamologep = new Szamologep();
		this.szamologep.setMuvelet("-");
		this.szamologep.setSzam1(3);
		this.szamologep.setSzam2(3);
		assertEquals(this.szamologep.szamol(), 0);
	}

}
