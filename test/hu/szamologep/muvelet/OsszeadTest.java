package hu.szamologep.muvelet;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import vs.szamologep.muvelet.Osszead;

class OsszeadTest {

	private Osszead osszead;
	private double a=3;
	private double b=4;
	
	@BeforeEach
	void setUp() {
		this.osszead = new Osszead(a,b);
	}
	
	@Test
	void constructorTest() {
		assertEquals(this.osszead.szamol(), (a+b));
		assertEquals(this.osszead.getA(), a);
		assertEquals(this.osszead.getB(), b);
		
	}
	
	@Test
	void setterMethodTest() {
		this.osszead.setA(-2);
		assertEquals(this.osszead.getA(), -2);
	}

}
