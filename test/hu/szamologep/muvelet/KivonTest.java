package hu.szamologep.muvelet;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vs.szamologep.muvelet.Kivon;

class KivonTest {
	
	private Kivon kivon = new Kivon(3,4);
	
	
	@Test
	void eset1() {
		assertEquals(this.kivon.szamol(), -1);
		
	}
	
	@Test
	void eset2() {
		this.kivon.setA(34);
		this.kivon.setB(10);
		assertEquals(this.kivon.szamol(), 24);

	}
	
	@Test
	void eset3() {
		this.kivon.setA(0);
		this.kivon.setB(0);
		assertEquals(this.kivon.szamol(), 0);

	}
	
	@Test
	void eset4() {
		this.kivon.setA(-1);
		this.kivon.setB(0);
		assertEquals(this.kivon.getA(), -1);
		assertNotEquals(this.kivon.getA(), 0);
		assertNotEquals(this.kivon.getA(), 1);
		
		assertEquals(this.kivon.getB(), 0);
		assertNotEquals(this.kivon.getB(), -1);
		assertNotEquals(this.kivon.getB(), 1);
		

	}

	@Test
	void eset5() {
		this.kivon.setA(-1);
		this.kivon.setB(-1);
		assertEquals(this.kivon.szamol(), 0);
		assertNotEquals(this.kivon.szamol(), -2);
		assertNotEquals(this.kivon.szamol(), -1);
		assertNotEquals(this.kivon.szamol(), 1);

	}
	
}
